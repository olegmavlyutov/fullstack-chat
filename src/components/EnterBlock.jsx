import React, {useState} from 'react';
import axios from 'axios';

function EnterBlock({onLogin}) {
    const [roomId, setRoomId] = useState('');
    const [userName, setUserName] = useState('');
    const [isLoading, setLoading] = useState(false);

    const onEnter = async () => {
        if (!roomId || !userName) {
            return alert('Wrong data!');
        }
        const obj = {
            roomId,
            userName
        };
        setLoading(true);
        await axios
            .post('/rooms', obj);  //to server
        onLogin(obj);
    };

    return (
        <div className='join-block'>
            <input
                type="text"
                placeholder="Room ID"
                value={roomId}
                onChange={e => setRoomId(e.target.value)}
            />
            <input
                type="text"
                placeholder="Your name"
                value={userName}
                onChange={e => setUserName(e.target.value)}
            />
            <button
                className='btn btn-success'
                disabled={isLoading}
                onClick={onEnter}>
                {
                    isLoading
                        ? 'Loading...'
                        : 'Enter'
                }
            </button>
        </div>
    )
}

export default EnterBlock;