import React, {useEffect, useReducer} from 'react';
import socket from './socket';

import EnterBlock from './components/EnterBlock';
import reducer from './reducer';
import Chat from './components/Chat'

function App() {
    const [state, dispatch] = useReducer(reducer, {
        joined: false,
        roomId: null,
        userName: null
    });

    const onLogin = (obj) => {
        dispatch({
            type: 'JOINED',
            payload: obj
        });
        socket.emit('ROOM:JOIN', obj);  //socket request to backend
    };

    useEffect(() => {
        socket.on('ROOM:JOINED', users => {
            console.log('new user', users);
        });
    }, [])


    return (
        <div className='wrapper'>
            {!state.joined
                ? <EnterBlock onLogin={onLogin}/>
                : <Chat/>
            }
        </div>
    );
}

export default App;
