const express = require('express');  //connect Express

const app = express();  //made application with express
const server = require('http').createServer(app);  //made HTTP server with our application
const io = require('socket.io')(server); //made sockets with HTTP server

app.use(express.json());  //our application can receive JSON data

const rooms = new Map();  //rooms created by users

app.get('/rooms', (req, res) => {  //show all rooms in JSON format
    res.json(rooms);
});

app.post('/rooms', (req, res) => {
    const {roomId, userName} = req.body;
    if (!rooms.has(roomId)) {
        rooms.set(
            roomId,
            new Map([
                ['users', new Map()],
                ['messages', []],
            ]),
        );
    }
    res.send();
});

io.on('connection', (socket) => {
    socket.on('ROOM:JOIN', ({roomId, userName}) => {
        socket.join(roomId);
        rooms.get(roomId).get('users').set(socket.id, userName);  //save userName in roomId-users
        const users = [...rooms.get(roomId).get('users').values()];  //get all userNames
        socket.to(roomId).broadcast.emit('ROOM:JOINED', users);  //send joined to all except userName
    })

    console.log('a user connected', socket.id)
});

server.listen(1234, (err) => {  //server works by 1234 port
    if (err) {  //show server error if something wrong
        throw Error(err);
    }
    console.log('Server is started on *:1234');  //message if everything good
});